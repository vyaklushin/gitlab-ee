# frozen_string_literal: true

require 'spec_helper'

RSpec.describe RemoteMirror do
  let(:project) { create(:project, :repository, :remote_mirror) }

  describe 'validations' do
    context 'when enable only_protected_branches and mirror_branch_regex ' do
      it 'is invalid' do
        remote_mirror = build(:remote_mirror, only_protected_branches: true, mirror_branch_regex: 'text')

        expect(remote_mirror).not_to be_valid
      end
    end

    context 'when disable only_protected_branches and enable mirror_branch_regex' do
      it 'is valid' do
        remote_mirror = build(:remote_mirror, only_protected_branches: false, mirror_branch_regex: 'test')

        expect(remote_mirror).to be_valid
      end
    end
  end

  describe '#sync' do
    let(:remote_mirror) { project.remote_mirrors.first }

    context 'as a Geo secondary' do
      it 'returns nil' do
        allow(Gitlab::Geo).to receive(:secondary?).and_return(true)

        expect(remote_mirror.sync).to be_nil
      end
    end
  end

  describe '#options_for_update' do
    let(:remote_mirror) { project.remote_mirrors.first }

    describe '#options_for_update' do
      let!(:match_branch) { create(:protected_branch, project: project, name: 'matched') }
      let!(:mismatch_branch) { create(:protected_branch, project: project, name: 'mismatched') }

      before do
        repository = project.repository
        masterrev = repository.find_branch("master").dereferenced_target.id
        parentrev = repository.commit(masterrev).parent_id
        repository.write_ref("refs/heads/matched", parentrev)
        repository.write_ref("refs/heads/mismatched", parentrev)
      end

      context 'when mirror_branch_regex is set' do
        it 'only sync matched branch' do
          mirror = build_stubbed(:remote_mirror, project: project, only_protected_branches: false,
                                                 mirror_branch_regex: '^matched*')

          options = mirror.options_for_update

          expect(options).to include(only_branches_matching: ['matched'])
        end
      end
    end
  end
end
