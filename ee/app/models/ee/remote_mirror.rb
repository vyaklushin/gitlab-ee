# frozen_string_literal: true

module EE
  module RemoteMirror
    extend ActiveSupport::Concern

    prepended do
      validates :mirror_branch_regex, absence: true, if: -> { only_protected_branches? }
    end

    def sync?
      super && !::Gitlab::Geo.secondary?
    end

    def options_for_update
      return super unless ::Feature.enabled?(:mirror_only_branches_match_regex)

      options = super

      if mirror_branch_regex.present?
        branch_filter = ::Gitlab::UntrustedRegexp.new(mirror_branch_regex)
        options[:only_branches_matching] = project.repository.branches.select do |branch|
          branch_filter.match?(branch.name)
        end.map(&:name)
      end

      options
    end
  end
end
